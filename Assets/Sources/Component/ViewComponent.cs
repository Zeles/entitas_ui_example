﻿using UnityEngine;
using Entitas;

[Game, Input, Ui]
public class ViewComponent : IComponent
{
    public GameObject view;
}
