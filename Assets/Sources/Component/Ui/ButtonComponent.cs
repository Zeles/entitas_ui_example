﻿using Entitas;
using UnityEngine.UI;

[Ui]
public class ButtonComponent : IComponent
{
    public Button button;
}
