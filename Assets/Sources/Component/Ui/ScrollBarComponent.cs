﻿using Entitas;
using UnityEngine.UI;

[Ui]
public class ScrollBarComponent : IComponent
{
    public Scrollbar scrollbar;
}
