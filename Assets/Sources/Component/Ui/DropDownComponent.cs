﻿using Entitas;
using UnityEngine.UI;

[Ui]
public class DropDownComponent : IComponent
{
    public Dropdown dropdown;
}
