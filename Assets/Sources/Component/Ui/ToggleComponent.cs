﻿using Entitas;
using UnityEngine.UI;

[Ui]
public class ToggleComponent : IComponent
{
    public Toggle toggle;
}
