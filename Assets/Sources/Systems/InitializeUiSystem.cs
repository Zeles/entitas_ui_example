﻿using System.Collections.Generic;
using System.Linq;
using Entitas;
using Entitas.Unity;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class InitializeUiSystem : IExecuteSystem
{
    private readonly UiContext uicontext;
 
    public InitializeUiSystem(Contexts contexts)
    {
        uicontext = contexts.ui;
    }

    public void Execute()
    {
        List<MenuInit> menuInits = Object.FindObjectsOfType<MenuInit>().ToList();
        foreach (MenuInit menuInit in menuInits)
        {
            var go = uicontext.CreateEntity();
            go.isMenu = true;
            go.AddView(menuInit.gameObject);
            go.AddUiKey(menuInit.gameObject.name);
            go.AddParent(go);

            List<UiInit> uiInits = Object.FindObjectsOfType<UiInit>().ToList();
            foreach (UiInit uiInit in uiInits)
            {
                GameObject parent = uiInit.gameObject.transform.parent.gameObject;
                while (parent == menuInit.gameObject)
                {
                    parent = parent.transform.parent.gameObject;
                }
                CreateUiItemParent(uiInit, go);
                
            }

            menuInit.gameObject.Link(go, uicontext);
            Object.Destroy(menuInit);
        }
    }

    void CreateUiItemParent(UiInit uiInit, Entity menu)
    {
        var go2 = uicontext.CreateEntity();
        go2.AddView(uiInit.gameObject);
        go2.AddUiKey(uiInit.gameObject.name);
        go2.AddParent(menu);

        switch (uiInit.UiKey)
        {
            case UiKey.button:
                go2.AddButton(uiInit.GetComponent<Button>());
                break;
            case UiKey.toggle:
                go2.AddToggle(uiInit.GetComponent<Toggle>());
                break;
            case UiKey.scrollBar:
                go2.AddScrollBar(uiInit.GetComponent<Scrollbar>());
                break;
            case UiKey.dropDown:
                go2.AddDropDown(uiInit.GetComponent<Dropdown>());
                break;
            case UiKey.inputField:
                go2.AddInputField(uiInit.GetComponent<InputField>());
                break;
            case UiKey.image:
                break;
            case UiKey.panal:
                break;
        }

        uiInit.gameObject.Link(go2, uicontext);
        Object.Destroy(uiInit);
    }
}
