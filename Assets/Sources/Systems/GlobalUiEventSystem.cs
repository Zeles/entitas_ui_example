﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class GlobalUiEventSystem : ReactiveSystem<UiEntity>
{
    private readonly UiContext context;

    public GlobalUiEventSystem(Contexts contexts) : base(contexts.ui)
    {
        context = contexts.ui;
    }

    protected override ICollector<UiEntity> GetTrigger(IContext<UiEntity> context)
    {
        return context.CreateCollector(UiMatcher.Click);
    }

    protected override bool Filter(UiEntity entity)
    {
        return entity.isClick && entity.hasUiKey && entity.hasParent;
    }

    protected override void Execute(List<UiEntity> entities)
    {
        foreach (UiEntity entity in entities)
        {
            switch (entity.uiKey.uiKey)
            {
                case "СloseMenuButton":
                    FindDestroyMenu(entity.parent.parent);
                    break;
            }
        }
    }

    void FindDestroyMenu(Entity menuEntity)
    {
        IGroup<UiEntity> entities = context.GetGroup(UiMatcher.AllOf(UiMatcher.Parent, UiMatcher.UiKey));
        foreach (UiEntity entity in entities)
        {
            if (entity.parent.parent == menuEntity)
            {
                entity.isDestroy = true;
            }
        }
    }
}