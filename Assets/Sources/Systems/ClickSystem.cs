﻿using System;
using System.Collections.Generic;
using Entitas;

public class ClickSystem : ReactiveSystem<UiEntity>, ICleanupSystem
{
    private readonly UiContext context;

    public ClickSystem(Contexts contexts) : base(contexts.ui)
    {
        context = contexts.ui;
    }

    protected override ICollector<UiEntity> GetTrigger(IContext<UiEntity> context)
    {
        return context.CreateCollector(UiMatcher.Button);
    }

    protected override bool Filter(UiEntity entity)
    {
        return entity.hasButton && entity.hasUiKey && entity.hasParent;
    }

    protected override void Execute(List<UiEntity> entities)
    {
        foreach (UiEntity entity in entities)
        {
            entity.button.button.onClick.AddListener(() =>
            {
                var go = context.CreateEntity();
                go.isClick = true;
                go.AddParent(entity.parent.parent);
                go.AddUiKey(entity.uiKey.uiKey);
            });
        }
    }

    public void Cleanup()
    {
        var destroyGroup = context.GetGroup(UiMatcher.Click);
        foreach (UiEntity entity in destroyGroup)
        {
            entity.isDestroy = true;
        }
    }
}