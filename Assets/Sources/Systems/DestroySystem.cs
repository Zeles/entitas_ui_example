﻿using UnityEngine;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using Object = UnityEngine.Object;

public interface IDestroy : IEntity, IDestroyEntity, IViewEntity { }

public partial class GameEntity : IDestroy { }
public partial class UiEntity : IDestroy { }
public partial class InputEntity : IDestroy { }

public class DestroySystem : MultiReactiveSystem<IDestroy, Contexts>
{
    public DestroySystem(Contexts contexts) : base(contexts)
    {
    }

    protected override ICollector[] GetTrigger(Contexts contexts)
    {
        return new ICollector[]
        {
            contexts.game.CreateCollector(GameMatcher.Destroy.Added()),
            contexts.ui.CreateCollector(UiMatcher.Destroy.Added()),
            contexts.input.CreateCollector(InputMatcher.Destroy.Added())
        };
    }

    protected override bool Filter(IDestroy entity)
    {
        return entity.isDestroy;
    }

    protected override void Execute(List<IDestroy> entities)
    {
        foreach (IDestroy entity in entities)
        {
            if (entity.hasView && entity.view.view != null)
            {
                GameObject go = entity.view.view;
                go.Unlink();
                Object.Destroy(go);
            }
            entity.Destroy();
        }
    }
}
