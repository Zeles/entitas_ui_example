﻿using UnityEngine;
using Entitas;

public class StartSystem : MonoBehaviour
{
    private Systems systems;
    private Contexts contexts;

    private void Start()
    {
        contexts = Contexts.sharedInstance;

        systems = new Feature()
            .Add(new Feature("Init")
                .Add(new InitializeUiSystem(contexts))
                .Add(new ClickSystem(contexts))
                .Add(new GlobalUiEventSystem(contexts)))
            .Add(new Feature("Destroy")
                .Add(new DestroySystem(contexts)))
            ;

        systems.Initialize();
    }

    private void Update()
    {
        systems.Execute();
        systems.Cleanup();
    }
}